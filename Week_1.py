# Starting Week 1 | Python 3.x.x

# Write a program to prompt the user for hours and rate per hour using input to compute gross pay.
# Pay the hourly rate for the hours up to 40 and 1.5 times the hourly rate for all hours worked above 40 hours.
# Use 45 hours and a rate of 10.50 per hour to test the program (the pay should be 498.75).
# You should use input to read a string and float() to convert the string to a number.
# Do not worry about error checking the user input - assume the user types numbers properly.
#_________________________________________________________________________________________
# hrs  = float(input("Enter Hours:"))
# rate = float(input("Enter Rate:"))
#
# if hrs > 40 :
#     otHrs = float(hrs - 40)
#     pay   = float((40 * rate) + (otHrs * rate * 1.5))
# else :
#     pay   = float(hrs * rate)
# print(pay)
#_________________________________________________________________________________________

# Write a program to prompt for a score between 0.0 and 1.0. If the score is out of range, print an error.
# If the score is between 0.0 and 1.0, print a grade using the following table:
# Score Grade
# >= 0.9 A
# >= 0.8 B
# >= 0.7 C
# >= 0.6 D
# < 0.6 F
# If the user enters a value out of range, print a suitable error message and exit. For the test, enter a score of 0.85.

#_________________________________________________________________________________________
# score = float (input("Enter a score between 0.0 and 1.0:"))
# if score < 0.0 or score > 1.0 :
#     print ("ERROR. Choose a number between 0.0 and 1.0.")
# elif score >= .9 :
#     print ("A")
# elif score >= .8 and score < .9 :
#     print ("B")
# elif score >= .7 and score < .8 :
#     print ("C")
# elif score >= .6 and score < .7 :
#     print ("D")
# else :
#     print ("F")
#_________________________________________________________________________________________

# Write a program to prompt the user for hours and rate per hour using input to compute gross pay.
# Award time-and-a-half for the hourly rate for all hours worked above 40 hours.
# Put the logic to do the computation of time-and-a-half in a function called computepay() and use the function to do the computation.
# The function should return a value. Use 45 hours and a rate of 10.50 per hour to test the program (the pay should be 498.75).
# You should use input to read a string and float() to convert the string to a number.
# Do not worry about error checking the user input unless you want to - you can assume the user types numbers properly.
# Do not name your variable sum or use the sum() function.

#_________________________________________________________________________________________
# def computepay(hrs, rate):
#     if hrs > 40 :
#         otHrs = float(hrs - 40)
#         pay   = float((40 * rate) + (otHrs * rate * 1.5))
#         return pay
#     else :
#         pay   = float(hrs * rate)
#         return pay
#
# hrs  = float(input("Enter Hours:"))
# rate = float(input("Enter Rate:"))
# print(computepay(hrs, rate))
#_________________________________________________________________________________________

# Write a program that repeatedly prompts a user for integer numbers until the user enters 'done'.
# Once 'done' is entered, print out the largest and smallest of the numbers.
# If the user enters anything other than a valid number catch it with a try/except and put out an appropriate message and ignore the number.
# Enter 7, 2, bob, 10, and 4 and match the output below.

#_________________________________________________________________________________________
# largest = None
# smallest = None
# while True :
#     num = input("Enter a number: ")
#     if num == "done" : break
#
#     try :
#         temp = int (num)
#     except :
#         print("Invalid input")
#         continue #go back to top
#
#
#     if largest is None or temp > largest :
#         largest = temp
#     else :
#         smallest = int (temp)
#
#
#
# print("Maximum is", largest)
# print("Minimum is", smallest)
#_________________________________________________________________________________________

#Write code using find() and string slicing (see section 6.10) to extract the number at the end of the line below.
#Convert the extracted value to a floating point number and print it out.

#_________________________________________________________________________________________
# text = "X-DSPAM-Confidence:    0.8475";
# endLen = len(text)
# count = text.find('0.8475') #start of 0 index
# value = text[count : endLen]
# print (float (value))
#_________________________________________________________________________________________

# Write a program that prompts for a file name, then opens that file and reads through the file,
# looking for lines of the form:
# X-DSPAM-Confidence:    0.8475
# Count these lines and extract the floating point values from each of the lines and
# compute the average of those values and produce an output as shown below.
# Do not use the sum() function or a variable named sum in your solution.
# You can download the sample data at http://www.py4e.com/code3/mbox-short.txt
# when you are testing below enter mbox-short.txt as the file name.

#_________________________________________________________________________________________
# Use the file name mbox-short.txt as the file name
# fname = input("Enter file name: ")
# fh = open(fname)
# count = 0
# total = 0
# for line in fh:
#     if not line.startswith("X-DSPAM-Confidence:") : continue
#     iline = str(line.rstrip())
#     endLen = len(iline)
#     begLen = iline.find('0.')
#     value = float (iline[begLen : endLen])
#     count += 1
#     total = total + value
#
#
# print ("Average spam confidence:" , total/count)
#_________________________________________________________________________________________

# Open the file romeo.txt and read it line by line. For each line, split the line into a
# list of words using the split() method. The program should build a list of words.
# For each word on each line check to see if the word is already in the list and if
# not append it to the list. When the program completes, sort and print the
# resulting words in alphabetical order.
# You can download the sample data at http://www.py4e.com/code3/romeo.txt

#_________________________________________________________________________________________
# fname = input("Enter file name: ")
# fh = open(fname)
# lst = list()
# for line in fh:
#     temp = (line.rstrip().split())
#     lst.append(temp)
#
# final = [item for sublist in lst for item in sublist]
# final = sorted(set(final))
# print(final)
#_________________________________________________________________________________________

# Open the file mbox-short.txt and read it line by line. When you find a
# line that starts with 'From ' like the following line:
# From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008
# You will parse the From line using split() and print out the
# second word in the line (i.e. the entire address of the person who sent the message).
# Then print out a count at the end.
# Hint: make sure not to include the lines that start with 'From:'.
# You can download the sample data at http://www.py4e.com/code3/mbox-short.txt

#_________________________________________________________________________________________
# fname = input("Enter file name: ")
# if len(fname) < 1 : fname = "mbox-short.txt"
#
# fh = open(fname)
# count = 0
#
# for line in fh :
#     if not line.startswith('From ') : continue
#     iline = (line.split())
#     print(iline[1])
#     count += 1
#
#
# print("There were", count, "lines in the file with From as the first word")
#_________________________________________________________________________________________

# Write a program to read through the mbox-short.txt and figure out who has the sent
# the greatest number of mail messages. The program looks for 'From ' lines and
# takes the second word of those lines as the person who sent the mail.
# The program creates a Python dictionary that maps the sender's mail address
# to a count of the number of times they appear in the file. After the dictionary
# is produced, the program reads through the dictionary using a maximum loop to find
# the most prolific committer.
#_________________________________________________________________________________________
# name = input("Enter file:")
# if len(name) < 1 : name = "mbox-short.txt"
# handle = open(name)
# count = 0
# d = dict()
# for line in handle :
#     if not line.startswith('From ') : continue
#     iline = (line.split())
#     email = (iline[1])
#     if email not in d :
#         d[email] = 1
#     else :
#         d[email]+= 1
#
# maximum = 0
# max_key = None
# for k in d:
#     if d[k] > maximum:
#         maximum = d[k]
#         max_key = k
#
# print(max_key, maximum)
#_________________________________________________________________________________________

# Write a program to read through the mbox-short.txt and figure out the distribution
# by hour of the day for each of the messages. You can pull the hour out from the
# 'From ' line by finding the time and then splitting the string a second time using a colon.
# From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008
# Once you have accumulated the counts for each hour, print out the counts,
# sorted by hour as shown below.
#_________________________________________________________________________________________
# name = input("Enter file:")
# if len(name) < 1 : name = "mbox-short.txt"
# handle = open(name)
# count = 0
# d = dict()
# for line in handle :
#     if not line.startswith('From ') : continue
#     iline = (line.split()[5].split(':')[0])
#     #print(iline)
#     if iline not in d :
#         d[iline] = 1
#     else :
#         d[iline] += 1
#
# lst = list()
# for key, val in list(d.items()) :
#     lst.append((val, key))
#     #lst.sort(reverse = True)
# hours  = sorted(lst, key=lambda x: x[1])
# for key, val in hours :
#     print (val, key)
#_________________________________________________________________________________________
